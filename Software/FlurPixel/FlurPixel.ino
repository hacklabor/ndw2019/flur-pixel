#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN D5


int Pix[]={0,15/3,45/3,30/3,60/3,75/3,105/3,90/3};
int laenge =5;
int module = 4;
int PixWidht= 4;
int PixHeigth= 2;
long LastTime = 0;
long NextPixelChange = 0;


Adafruit_NeoPixel strip = Adafruit_NeoPixel(module*laenge*PixHeigth, PIN, NEO_RGB + NEO_KHZ800);

void setup() {
	Serial.begin(115200);
	delay(50),
  strip.begin();
  allPixel(255, 0, 0);
  delay(10000);
  allPixel(0, 255, 0);
  delay(10000);
  allPixel(0, 0, 255);
  delay(10000);

  strip.show(); // Initialize all pixels to 'off'
  NextPixelChange = (random(30, 3000));
  Serial.println("Start");
}

void loop() {
  // Some example procedures showing how to display to the pixels:
	delay(10);
	//if (millis() > 600000) { ESP.reset; }
	if (millis() - LastTime > NextPixelChange) {
		
		NextPixelChange = (random(30, 1000));
		Serial.println(NextPixelChange);
		LastTime = millis();
		for (int z = 0; z < 4; z++) {
			for (int s = 0; s < 2; s++) {
				RandomColor(z, s);
			}
		}
		
}


  
}

void RandomColor(uint8_t x1, uint8_t y1)
{
	Serial.println("X: "+String(x1)+" Y: "+String(y1));
	if (random(0, 100) > 40) { return; }
	uint8_t g = random(100);
	uint8_t r = random(100);
	uint8_t b = random(100);
	if (g > 50) { g = 255; } else{ g = 0; }
	if (r > 50) { r = 255; } else { r = 0; }
	if (b > 50) { b = 255; } else { b = 0; }
	if ((r == 0) && (b == 0) && (g == 0)) { r = 255; g = 255; }
	Serial.println("fARBE");
	Serial.println(r);
	Serial.println(g);
	Serial.println(b);
	Pixel(x1, y1, r, g, b);
	strip.show();

}

// Fill the dots one after the other with a color


void Pixel(uint8_t x, uint8_t y, uint8_t rot, uint8_t gruen, uint8_t blau) {
   int Start = Pix[x*2+y];
  
  for(uint16_t i=Start; i<Start+laenge; i++) {
    strip.setPixelColor(i, rot, gruen,blau);
    
  }
}

void allPixel(uint8_t r,uint8_t g,uint8_t b){
  for (int x1=0; x1<4; x1++){
  
  Pixel(x1,0,r,g,b);
 Pixel(x1,1,r,g,b);
}
strip.show();
}
